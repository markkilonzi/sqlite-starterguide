package com.example.marko.sqlitetutorial;

import android.database.Cursor;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

public class MainActivity extends AppCompatActivity {

    EditText addName, addMarks, addId,addSurname;
    Button btnViewAll,btnaddData, btnUpdateData, btnDeleteData;

    DatabaseHelper myDB;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        addName = (EditText) findViewById(R.id.edit_name_text);
        addMarks = (EditText) findViewById(R.id.edit_marks_text);
        addSurname = (EditText) findViewById(R.id.edit_surname_text);
        addId = (EditText) findViewById(R.id.edit_id_text);
        btnaddData = (Button) findViewById(R.id.add_data_button);
        btnViewAll = (Button) findViewById(R.id.show_data_button);
        btnUpdateData = (Button) findViewById(R.id.update_data_button);
        btnDeleteData = (Button) findViewById(R.id.delete_data_Button);

        myDB = new DatabaseHelper(this);
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();

        actualAddData();
        viewAll();
        UpdateData();
        DeleteData();
    }

    public void actualAddData() {

        btnaddData.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        boolean isInserted = myDB.insertData(addName.getText().toString(),addSurname.getText().toString(),addMarks.getText().toString());

                        if(isInserted){
                            Toast.makeText(MainActivity.this, "Data Inserted!", Toast.LENGTH_LONG).show();
                        }
                        else{
                            Toast.makeText(MainActivity.this, "Sorry, data not inserted!", Toast.LENGTH_LONG).show();
                        }
                    }
                }
        );
    }

    public void viewAll(){
        btnViewAll.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                       Cursor res =  myDB.getAllData();

                        if(res.getCount() == 0){

                            showMessage("Error!", "No data found");
                            return;
                        }

                        StringBuilder buffer = new StringBuilder();

                        while(res.moveToNext()){
                            buffer.append("Id :" + res.getString(0) + "\n");
                            buffer.append("Name :" + res.getString(1) + "\n");
                            buffer.append("Surname :" + res.getString(2) + "\n");
                            buffer.append("Marks :" + res.getString(3) + "\n\n");
                        }

                        showMessage("Data", buffer.toString());
                    }
                }
        );
    }
    public void showMessage(String title, String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.show();
    }

    public void UpdateData(){
        btnUpdateData.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        boolean isUpdated = myDB.updateData(addId.getText().toString(),addName.getText().toString(),addSurname.getText().toString(), addMarks.getText().toString() );

                        if(isUpdated){
                            Toast.makeText(MainActivity.this, "Data Updated!", Toast.LENGTH_LONG).show();
                        }else{
                            Toast.makeText(MainActivity.this, "Sorry, data not updated!", Toast.LENGTH_LONG).show();

                        }
                    }
                }
        );
    }

    public void DeleteData(){
        btnDeleteData.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Integer deletedRows = myDB.deleteData(addId.getText().toString());

                        if(deletedRows > 0){
                            Toast.makeText(MainActivity.this, "Data Deleted!", Toast.LENGTH_LONG).show();

                        }else{
                            Toast.makeText(MainActivity.this, "Sorry, data not deleted!", Toast.LENGTH_LONG).show();

                        }
                    }
                }
        );
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Main Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://com.example.marko.sqlitetutorial/http/host/path")
        );
        AppIndex.AppIndexApi.start(client, viewAction);
    }


    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Main Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://com.example.marko.sqlitetutorial/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        client.disconnect();
    }
}
